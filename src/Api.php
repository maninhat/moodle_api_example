<?php

namespace App;


use App\Moodle\Connection;

class Api
{
    protected Connection $connection;
    private Factory $factory;

    public function __construct(string $moodleEndpoint, string $moodleToken)
    {
//        die(var_dump($moodleEndpoint,$moodleToken));
        $this->factory = new Factory();
        $this->connection = new Connection($moodleEndpoint, $moodleToken);
    }

    private function findUser(string $username): ?array
    {
        $users = $this->connection->getUsersByField([$username], $this->connection::SEARCH_FIELD_USERNAME);
        if (!empty($users[0])) {
            return $users[0];
        }
        return null;
    }

    public function createIfNeed(string $email): ?array
    {
        $userArray = $this->factory->build($email, '123456'); //Пароль должен быть уникальным
        $moodleUser = $this->findUser($userArray['username']);
        if (isset($moodleUser['id'])) return [
            'id' => $moodleUser['id'],
            'username' => $moodleUser['username'],
        ];
        $userResult = $this->connection->createUser($userArray);

        return [
            'id' => $userResult[0]['id'],
            'username' => $userResult[0]['username'],
        ];
    }

    public function enrollUser(int $user_id, array $courses): bool
    {
        try {
            $res = $this->connection->enrollUserToCourse($user_id, $courses);
//        dump($res);
        } catch (\Exception $e) {
            return false;
        }
//dump($groups_result);

        return true;
    }

    public function getAuthLink(string $username): ?string
    {
        return $this->connection->getAuthLink($username);
    }
}
