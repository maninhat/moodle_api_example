<?php
namespace App;
require_once '../vendor/autoload.php';
$email='test4@mail.ru';
$api = new Api(getenv('MOODLE_ENDPOINT'), getenv('MOODLE_TOKEN'));
//die(var_dump($api));


$moodleUser = $api->createIfNeed($email);
$moodleCourse=2;  //ID курса в мудл
$api->enrollUser($moodleUser['id'], [$moodleCourse]);

$login_url = $api->getAuthLink($moodleUser['username']);


echo $login_url.sprintf('&wantsurl=https://exam-universiada.rudn.ru/course/view.php?id=%d',$moodleCourse);
