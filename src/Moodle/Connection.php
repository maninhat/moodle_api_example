<?php

namespace App\Moodle;


class Connection
{
    public const ROLE_STUDENT = 5;
    public const ROLE_TEACHER = 4;

    private const ENDPOINT = '/webservice/rest/server.php';
    private const COURSE = '/course/view.php?id=%d';

    public const SEARCH_FIELD_USERNAME = 'username';
    private \MoodleRest $con;
    private string $moodleDomain;

    public function __construct(string $endpoint, string $token)
    {
        $this->con = new \MoodleRest($endpoint . self::ENDPOINT, $token);
        $this->moodleDomain = $endpoint;
    }

    public function getGroups(array $ids)
    {
        $groups = $this->con->request('core_group_get_groups', array('groupids' => $ids));
        return $groups;
    }

    /**
     * @throws \Exception
     */
    public function getUsers(array $ids, string $field = 'id')
    {
//        core_user_get_users_by_field&field=id&values[0]=2
        $groups = $this->con->request(
            'core_user_get_users ',
            [
                'criteria' => array_map(function ($value) use ($field) {
                    return [
                        'key' => $field,
                        'value' => $value,
                    ];
                }, $ids)]
        );

        $this->checkError($groups);
        return $groups;
    }

    /**
     * @throws \Exception
     */
    private function checkError(?array $response)
    {
        if (isset($response['exception'])) {
            var_dump($response);
            throw new \Exception($response['exception']);
        }
    }

    public function getUsersByField(array $values, string $field = self::SEARCH_FIELD_USERNAME)
    {
//        core_user_get_users_by_field&field=id&values[0]=2
        $groups = $this->con->request(
            'core_user_get_users_by_field ',
            [
                'field' => $field,
                'values' => $values
            ]
        );
//        $groups = $this->con->request('core_user_get_users_by_field', array('field' => $field, 'values' => $ids));
        $this->checkError($groups);
        return $groups;
    }

    /**
     * @throws \Exception
     */
    public function createUser(array $user)
    {
        $res = $this->con->request('core_user_create_users', ['users' => [$user]]);
        $this->checkError($res);
        return $res;
    }

    /**
     * @throws \Exception
     */
    public function deleteUser(int $id)
    {
        $res = $this->con->request('core_user_delete_users', ['userids' => [$id]]);
        $this->checkError($res);
        return $res;
    }

    /**
     * @throws \Exception
     *
     */
    public function assignUserToGroups(?int $userId, array $groups_assign)
    {
        $data = array_map(function ($group) use ($userId) {
            return [
                'groupid' => intval($group),
                'userid' => intval($userId),
            ];
        }, $groups_assign);
//        die(var_dump($data));
        $data = ['members' => $data];
//        dump($data);
        $res = $this->con->request('core_group_add_group_members', $data);
        $this->checkError($res);
        return $res;
    }

    /**
     * @throws \Exception
     *
     */
    public function enrollUserToCourse(?int $userId, array $courses)
    {
        $data = array_map(function ($c) use ($userId) {
            return [
                'courseid' => intval($c),
                'userid' => intval($userId),
                'roleid' => self::ROLE_STUDENT,
            ];
        }, $courses);
//        die(var_dump($data));
        $data = ['enrolments' => $data];
//        dump($data);
        $res = $this->con->request('enrol_manual_enrol_users', $data);
        $this->checkError($res);
        return $res;
    }

    public function getReview($email)
    {
        return $this->con->request('availability_examus_user_proctored_modules', ['useremail' => $email]);
    }

    public function bestGrade($user_id, $quiz_id)
    {
        return $this->con->request('mod_quiz_get_user_best_grade', ['quizid' => $quiz_id, 'userid' => $user_id]);
    }

    public function getAuthLink($user_id)
    {
        $res = $this->con->request('auth_userkey_request_login_url', [
            'user' => [
                'username' => $user_id,
//                'email'=>$user_id.'@local.local',
            ]
        ]);
        $this->checkError($res);
        if (!empty($res['loginurl'])) {
            return $res['loginurl'];
        }
        return null;
    }

    public function getCourseUrl(int $course_id): string
    {
        return sprintf($this->moodleDomain . self::COURSE, $course_id);
    }

    //core_course_get_course_module

    /**
     * @throws \Exception
     */
    public function getCourses(array $ids)
    {
        $res = $this->con->request('core_course_get_courses', ['options' => [
            'ids' => $ids
        ]]);
//        dump($res);
        $this->checkError($res);

        return $res;
    }
}
