<?php

namespace App;

class Factory
{

    public function build(string $email, string $password): array
    {
        return [
            'createpassword' => 0,
            'username' => $email,  // У себя я использую уникальные имена пользователей, для примера email
            'auth' => 'manual',
            'password' => $password,
            'firstname' => $email,
            'lastname' => $email,
            'email' => $email,
            'maildisplay' => 0,
            'lang' => 'ru',

        ];
    }
}
